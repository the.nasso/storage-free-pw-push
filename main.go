package main

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/md5"
	"crypto/rand"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"os"
	"time"
)

const encryptionKey = "its secret"

type payload struct {
	Secret   string `json:"s"`
	Expire   int64  `json:"e"`
	Checksum string `json:"c"`
}

func main() {
	var decodeData = flag.String("d", "", "data to decode")
	flag.Parse()
	encodeData := ""
	if len(flag.Args()) == 1 {
		encodeData = flag.Arg(0)
	}
	if *decodeData != "" {
		decode(*decodeData)
	} else if encodeData != "" {
		encode(encodeData)
	}
}

func encode(data string) {
	expire := time.Now().Unix()

	p := payload{
		Secret:   data,
		Expire:   expire + 15,
		Checksum: createHash(data + string(expire)),
	}

	j, _ := json.Marshal(p)
	enc := encrypt(j)
	b64 := base64.StdEncoding.EncodeToString(enc)
	fmt.Println(b64)
}

func decode(data string) {
	j, _ := base64.StdEncoding.DecodeString(data)
	dec := decrypt(j)
	p := payload{}
	_ = json.Unmarshal(dec, &p)

	checksum := createHash(p.Secret + string(p.Expire))

	if checksum != p.Checksum {
		fmt.Println("Invalid checksum")
		os.Exit(1)
	}

	if time.Now().Unix() > p.Expire {
		fmt.Println("Expired")
		os.Exit(1)
	}

	fmt.Printf("%v", p)
}

func encrypt(data []byte) []byte {
	block, _ := aes.NewCipher([]byte(createHash(encryptionKey)))
	gcm, err := cipher.NewGCM(block)
	if err != nil {
		panic(err.Error())
	}
	nonce := make([]byte, gcm.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		panic(err.Error())
	}
	ciphertext := gcm.Seal(nonce, nonce, data, nil)
	return ciphertext
}

func decrypt(data []byte) []byte {
	key := []byte(createHash(encryptionKey))
	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err.Error())
	}
	gcm, err := cipher.NewGCM(block)
	if err != nil {
		panic(err.Error())
	}
	nonceSize := gcm.NonceSize()
	nonce, ciphertext := data[:nonceSize], data[nonceSize:]
	plaintext, err := gcm.Open(nil, nonce, ciphertext, nil)
	if err != nil {
		panic(err.Error())
	}
	return plaintext
}

func createHash(key string) string {
	hasher := md5.New()
	hasher.Write([]byte(key))
	return hex.EncodeToString(hasher.Sum(nil))
}
